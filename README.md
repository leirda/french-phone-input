# French Phone Input

![Licensed under the WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png)

Prompt for a french phone number and interactively format it with spaces.
