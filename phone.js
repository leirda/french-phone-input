'use strict';
/*
 * [...] It makes you wonder if it is worth going to all this trouble on the
 * client-side, when you could just let the user enter their number in whatever
 * format they wanted on the client-side and then validate and sanitize it on
 * the server. But this choice is yours to make.
 *
 * from https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/tel
 */

const removeNonDigitChars = (string) => string.replace(/\D/g, "");

const removeTrailingChars = (string) => string.startsWith("0")
    ? string.slice(0, 10)
    : string.slice(0, 9);

const addSpaces = (string) =>
    string.replace(/0?\d/, "$& ")
        .replace(/\d\d(?! )/g, "$& ")
        .trimEnd();

// https://1loc.dev/#compose-functions-from-left-to-right
const pipe = (...fns) => (x) => fns.reduce((v, f) => f(v), x);

const validate = pipe(
    removeNonDigitChars,
    removeTrailingChars,
    addSpaces,
);

// TODO: Be less imperative, and more pure!
document.getElementById("phone").oninput = (e) => {
    const oldCaret = e.target;
    e.target.value = validate(e.target.value);
    const restoreCaret = oldCaret.selectionStart + (e.target.value.length - oldCaret.value.length);
    e.target.setSelectionRange(restoreCaret, restoreCaret);
};
